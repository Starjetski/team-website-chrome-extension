var defaultRepositories = [
    {
        stashPath: '/projects/BES/repos/beslist/',
        localPath: 'c:\\dev.beslist.nl\\'
    },
    {
        stashPath: '/projects/TWEB/repos/statistics-api/',
        localPath: 'c:\\dev.statistics-registration.api.beslist.nl\\'
    },
    {
        stashPath: '/projects/TWEB/repos/shoppingcart-api/',
        localPath: 'c:\\shoppingcart-api\\'
    },
    {
        stashPath: '',
        localPath: ''
    },
    {
        stashPath: '',
        localPath: ''
    },
    {
        stashPath: '',
        localPath: ''
    },
    {
        stashPath: '',
        localPath: ''
    },
    {
        stashPath: '',
        localPath: ''
    },
    {
        stashPath: '',
        localPath: ''
    },
    {
        stashPath: '',
        localPath: ''
    },
    {
        stashPath: '',
        localPath: ''
    }
];

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {

    chrome.storage.sync.get({
        repositories: defaultRepositories
    }, function (items) {
        var rows = items.repositories.map(function (repository, index) {
            return addRepositoryRowToTable(repository, index)
        });
        var repositoriesContainer = document.getElementById('repositories');

        repositoriesContainer.innerHTML = rows.join('');
    });
}

// Saves options to chrome.storage
function save_options() {
    var repositories = [];
    var repositoryRows = document.getElementsByClassName('repository_row');


    for (var i = 0; i < repositoryRows.length; i++) {
        var stashPath = document.getElementById(`repository_${i}_stash_path`).value;
        var localPath = document.getElementById(`repository_${i}_local_path`).value;

        repositories.push({
            stashPath: stashPath,
            localPath: localPath
        });
    }

    console.log(repositories);

    chrome.storage.sync.set({
        repositories: repositories
    }, function () {
        // Update status to let user know options were saved.
        var status = document.getElementById('status');
        status.textContent = 'Options saved.';
        setTimeout(function () {
            status.textContent = '';
        }, 1750);
    });
}

function addRepositoryRowToTable(repository, index) {
    return `
        <tr id="repository_${index}" class="repository_row">
            <td class="stash_path"><input type="text" id="repository_${index}_stash_path" name="url_pattern" value="${repository.stashPath}"></td>
            <td class="local_path"><input type="text" id="repository_${index}_local_path" name="local_path" value="${repository.localPath}"></td>
        </tr>
    `;
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);