'use strict';
modifyFaviconOnBuildStatusChange();

chrome.storage.sync.get({
    repositories: []
}, function (items) {
    addEventHandlerForLineNumberClick(items.repositories);
});


function modifyFaviconOnBuildStatusChange() {
    let redBucket = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAmVBMVEX////8/P72+Prt8fXk6fDf5u7W3+jR2+XG0t/Bzty8y9nM1uLb4+vp7fLy9fj5+vy4x9ecsceFn7pzkbBegaVNc5s7ZZAsWYgiUoLtHCQgUIEmVYUxXYtHb5dafqJri6yBnLiTqsKrvdCwwdOMpb5oiaqkt8w2YY5khahvjq5WeqCzw9WYrsV5lbNCa5SgtMqHobxRdp19mbYxyOkCAAAAAXRSTlMAQObYZgAABYdJREFUeAG92ufCqjoXBOChvIBKU+oCUbGLgsr939tXztndQELZPD/tJaOLIehJkhX1S9ON2dxYaKZq2Q4moOjucuX5QRjF9CFJ19lmu8v3hupgbLJ2OJ6ylATFob87GwpG4ehnL4upj8s1n9kYwlnkfkSDJNnOtdGLfbhFNIq4WJroSL5fYxpT9rAgziwjGl2ycSUIMfyE/o7qKYNL3xBfvA6DYuNfT1vP256u/qYIwighvsteQivbS6hREha343muqTaY7C99/nzdirDtpWQaWhghsUWb1V6XIUjW97tNRGxxLqHJPiaG1H9q6EF7XlNiOTlgWxLDe2+jN/nOXNCFA5YZ47bBAgOZPn0qwSCv6cPWwXCsD3YudrtUxhiujC8Wn970IZIwhpo+qfiwpk8vjOCLFcgZPqTEcLIw1H1NYougIpa01DGAfAiIScMHnxpUL8NBH0rLRGG3LBWGtHjtNQfi7MW5zBJqtManJ3HE1Xb1dDUFLSRrcV/u/JA4WDGckaD0khVXb3XMH8vzfn/Yn5/LR36sb9eiusQkxsMnlSb0AENK03HBENB0TDDcaDKpA4acJlOBxaXJnMBi0mSOYHFimsoBTBVNRQPTiSYSO2A6ThoChjtN5Ao2bfoQTB8DdgimjoGGBqdJQ8BwnDQEDPdJQ8BgThuC4TGIMr+sT+8LdXNHo4zEhTtDwr++lkXSIwQMW+Jjt03aZoQQiA9FcS7jT7NwaAjEh6LIAINSCI9DA2MQfoHJ8QaGAJBS4otMNJB84WOCATGIZ2hkV8RnosVt2CcILSaeVAKGxGBt4wdnttpey72CnzziydBmTjxP/HD4Frz09fNFWSlx3NBGJY61w1rzmYrvSuLI0Sqidh77mSpbuGaYo1UgmiG34YN1ImqnopVHrWK5YXxLNMGxKhUtdtku+EZv7DRX1CpAu5ng3R+N1yxFFxGbJThN1Y2fzV24HWJbi/2TlX1fwAwchVi/l/f9CizRbp0txDcG/WGHb17UZg2es1i7JF3odwuxsa4Az0LwK9zTb3x8I62pTQ0em1rV7BMxF1XwDZzBdRH7JYLt0w8XDYK/Qwtw+aJHFVIe0T+SrfXzZUXUygbXilqFDn5Qzqcg83NT/N4X8O2p3RIt1JRa+eDTqV2qoZHzpnYr8MkJtbtYaOIRxx4CKuIIlN4Fhw4BHvGEJhjkG/FEDgTMiCs64IMZEJcHIT7xBQZ+Y5UxcUUWhCghCSieKr6RXS8lvsSFIPNCQrJTnS9ftyIlEckZwr4qGl16QAf2iUYW6uhmv6YRJbWNrpQypuEG7YUxTwmNoXLRl1mmNFThYghlWdEAUa1jsMUupF7S013GOPTjO6ZuwtKVMSbbfRUpCUky7/yFgQ4eY+uhpB2O1yqmZpeifhqMyC/qo40udCLaqM37RB7HcrsJsqoKw7DKguLqrfK98SWDyTnG/GmMcYAWLR2MYZGJHZQxhpJqjsHULXMYEe0pCgODqHXKHufFe4q3K6Ev7ZdBxYAYxlgW5hZ6cO4b+oUCIeyaIb4ebHSzqNfsakPQgf6QXs8WBMnz+sKut8SZxJDVdwscsnHcpMItf49zJxf/dddtMDjm/HGrYmJz0dGbd8rytsuX5/3hsD8/H6/y+r4kAypihpJGFaGrM42qQFc6jWqHruSYxrRHZxWNSUNnt/H373WzpBEF6M6gEZXozk5oPGf0ENJ4dPSwnWDn0FSrMEAf6t/axzz9dxAp6EWNpssA2zyhMXjo7R7TcDcH/RlrGih5SRjC8mmQywxD3UPqLV3JGM45961oagvjkNxNQl2FDwUjss6bmMRVRx2js/fbkAREm9zE36LMj35LMtOg3pv462Rzdn5tN++sukRpHK3D/5dE9dLVFXT3X8h3l6vmw9tGAAAAAElFTkSuQmCC";
    let greenBucket = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAbFBMVEX////8/P74+vzt8fXj6PDW3+jP2eTG0t+/zdvb4+vp7fLy9fi4x9edssiGoLtxkK9egaVOdJw7ZZAsWYghUYIisUwmVYU0X41Hb5dXfKFpiqt+mreVrMOrvdCywtSMpb6kt8xkhah5lbNCa5ReHAF3AAAAAXRSTlMAQObYZgAABTVJREFUeAG92udiqzwMBmBhdthgYcwe93+N3x49jQgCXJ5/ZxaPN5GF4SJL2I7jen4Q+J4bOq9IwANsL07SLC/KSuIbWatGt10/+I4A04Q7Tm1TI5Ms8272bTBCeHPWSLxCLX0Q3fzhfV7hLbLp4gguicasQiOkTkI4SYyLRJOa/gV84VqhcVLHFrD4ucSfsSUCDnmaMxpVFlrnS9tmWdsuudbUZwNBDRZ8FGXyU7x1Ns2B60RAihwvSNJMlxL3NS584JdIq3Q6eIIf36HTFdJkb8GeQSKhzhMXLnCTpUZKK4CWIKEYIrhMjOSG1gIogSR+vAc3hTm+W4EgFDlZd9ETG/D+Xi3AhIWYWXhX4JvKAhM6fOfAG4XvUjDAqXhrUCOhfcFdo0LeA2xIqVcPbhBjgSQX3uS4Y0t9AVfYHyqKaH+rUGqdDq4Avsib10+FnDqMIUFubZrErg0fWC9vTLq8RMJRDANkqlWjlyyd+j6Zh2Ec5iTp+6nLFr0piTwZlRZ8UA+EGp8TA6HA54RAyPAxtQBCj4/ZgBLjY1qghPiYCShC4lNGIG34FBdILT5ECiBNj4aAMOJDFqC5T4eAjsHzIXg+Bi7saB8NAWF6NASE8dEQEMJnQ3A/BlWTr11bqIshIDTIV3a+BX9zEi3vhoCOAb/b5GoDIeAXRbIX8F1Q3g4BuyiqfCDYml0O3YxB6QBJZOdCQLBqzvhD2GHlzDPBrRjIAHZFG/9MQMvuzSC4Eo/UFsCdGKgI/iOCtF3WwT41ggY+CfBIAv8Zy3/GlP7/UK/61MH4/BFZCWrPN//nYuUfjEkV4/mJn7RF7DkM4KOCm6F458HE0RAcgBsxkGKnfJMu8/uk5jR2Gc0lb7enmfK7QxdiUOzmtWAOIYPPXsxqqtudm5EfApLifZOtVx8ggAP6h5fgxe+tU0r4h4/fdLxNqODIzOsuWd+XyuPFUMMRj7mEA/4iB+LJCB0cifCjjn4RoxzmAGY4pJhrGOVffheB+TnkwaGce6qw+gr/Ir+8V4kq/CiCPdwhlAL+Y89t0eR9yP/XCo4N7IqE4NT4UQ5wNwZYu7BLFPhZCoSzJ1T1ul7TDsCw4YHCvtzg8IAhwyNlePFgVAlgCPBQNcKbsMBDGbDkeKzw4RevVTKe+wUsdokMOnHgHyLOajwmY2AKFbI0bdcnaaZr5JAzsDkbGlePcELUomGlB+cMCg2SXQRn2avE+27dhQlbiSZsMVwVrjXepWO4w05uBaLqPLjN60q8pG5HAWZ4UyHxnHKNBZgUxamukUU22ezATWNGXD203HFaNon7lO4SP6KWcYrgDA8RtbN/T6Sf1lYXzbaVZbk1hV6y9MOVajFJoho7PqBViYBzLKB4DXEo4xUlWwC3OS2zGKH7FNqHW5yupst5/gGtiC24yv1SqHBHQpRlZf+CC8So8QsbCNwDmlzG6PTnp6JbG0wjflMv8wuYRNApur3FFyKh6cbDhxD+pGt+l//8uxOVp6MXAUGEQZ9tEmkxnFQcvbLMuj6Zh3Ec5qRP16VQkt8i5ljRqArOmtEoDWd5aFQHZwmJJg1w2oYmuXBaZv7+3jkJGlTAeT4atMJ5kURzZrigRHM8uKB94ubQQ7uwgCucH77H/OAaVDZc4lSPZoAQSDQhg8tGifdlAq7zFd4kUwvueOV4iwrgrrHEy+pUwH1ivtqi6V5ghhVriWeVvQ0GveZTz7BNHhgXDW2JDJXuQ/gpdjDlCnfVRTeY/uH0GWxO/2oRqaqWlSqbQi9dEns2nPc7LiL4fq6SdcEAAAAASUVORK5CYII=";

    let singlePullRequestPattern = RegExp("\/pull-requests\/\\d*\/overview");
    if (singlePullRequestPattern.test(location.href)) {
        // console.log('add event handler');
    }
}


function addEventHandlerForLineNumberClick(repositories) {
    jQuery('body').on('click', '.line-number-to', function (e) {

        let repositoryPaths = repositories;

        let $lineNumber = jQuery(e.currentTarget);
        let $filetoolbar = $lineNumber.closest('.file-content').find('.file-toolbar .breadcrumbs');
        let file = $filetoolbar.text();
        let line = $lineNumber.text();
        let href = 'phpstorm://open?url=file://' + getRepositoryPath() + file + '&line=' + line;

        location.href = href;

        function getRepositoryPath() {

            let repository = repositoryPaths.find(function (item) {
                return location.pathname.startsWith(item.stashPath);
            });

            return repository && repository.localPath || '';
        }
    });
}

